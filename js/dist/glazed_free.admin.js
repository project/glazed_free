(function ($, Drupal) {
  /*global jQuery:false */
  /*global Drupal:false */
  "use strict";

  /**
   * Provide vertical tab summaries for Bootstrap settings.
   */
  Drupal.behaviors.glazed_freeAdmin = {
    attach: function (context) {
      var $context = $(context);
      var $toolbar = $('#toolbar, #navbar-bar, #admin-menu', context);
      if (($toolbar.length > 0) && (Drupal.settings.glazed_free.glazed_freePath)) {
        glazed_freeButtonAdd($toolbar);
      }
      /**
       * Hook into Admin Menu Cached loading
       */
      if ('admin' in Drupal) {
        Drupal.admin.behaviors.glazed_freeButton = function (context, settings, $adminMenu) {
          $toolbar = $('#admin-menu', context);
          glazed_freeButtonAdd($toolbar);
        };
      }

      function glazed_freeButtonAdd($toolbar) {
        var themeName = Drupal.settings.glazed_freeDefaultTheme || 'glazed_free';
        var glazed_freeLogoPath = Drupal.settings.basePath + Drupal.settings.glazed_free.glazed_freePath + '/logo.svg';
        var $glazed_freeButton = $('<div id="glazed_free-button-wrapper">').html($('<a>',{
          text: Drupal.t('Theme Settings'),
          title: 'Theme Settings, Demo Import, and more',
          class: 'glazed_free-button',
          href: Drupal.settings.basePath + 'admin/appearance/settings/' + themeName
        }).prepend($('<img>',{
          src: glazed_freeLogoPath,
          width: 15
        })));
        $('.toolbar-menu, #admin-menu-wrapper', $toolbar).once('glazed_free_button').append($glazed_freeButton);
      }
    }
  };
})(jQuery, Drupal);
