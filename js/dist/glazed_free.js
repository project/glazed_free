/**
 * @file
 * A JavaScript file that styles the page with bootstrap classes.
 *
 * @see sass/styles.scss for more info
 */
(function ($, Drupal, window, document, undefined) {
var glazed_freeMenuState = '';

Drupal.behaviors.fullScreenSearch = {
    attach: function(context, settings) {
        function clearSearchForm() {
            $searchForm.toggleClass("hidden"),
            $('body').toggleClass("body--full-screen-search"),
            setTimeout(function() {
                $searchFormInput.val("")
            }, 350)
        }
        var $searchButton = $(".full-screen-search-button")
          , $searchForm = $(".full-screen-search-form")
          , $searchFormInput = $searchForm.find(".search-query")
          , escapeCode = 27;
        $searchButton.on("touchstart click", function(event) {
            event.preventDefault(),
            $searchForm.toggleClass("hidden"),
            $('body').toggleClass("body--full-screen-search"),
            $searchFormInput.focus()
        }),
        $searchForm.on("touchstart click", function($searchButton) {
            $($searchButton.target).hasClass("search-query") || clearSearchForm()
        }),
        $(document).keydown(function(event) {
            event.which === escapeCode && !$searchForm.hasClass("hidden") && clearSearchForm()
        })
    }
}

Drupal.behaviors.glazed_free = {
  attach: function(context, settings) {
    var windowHeight = $(window).height();
    if ($('#glazed_free-main-menu .menu').length > 0) {
      glazed_freeMenuGovernor(document);
    }

    // Helper classes
    $('.glazed-util-full-height', context).css('min-height', windowHeight);

    // User page
    $('.page-user .main-container', context).find('> .row > .col-sm-12')
        .once('glazed_free')
        .removeClass('col-sm-12')
        .addClass('col-sm-8 col-md-offset-2');

    // Main content layout
    $('.glazed-util-content-center-4-col .main-container', context).find('> .row > .col-sm-12')
        .once('glazed_free')
        .removeClass('col-sm-12')
        .addClass('col-sm-4 col-md-offset-4');

    $('.glazed-util-content-center-6-col .main-container', context).find('> .row > .col-sm-12')
        .once('glazed_free')
        .removeClass('col-sm-12')
        .addClass('col-sm-6 col-md-offset-3');

    $('.glazed-util-content-center-8-col .main-container', context).find('> .row > .col-sm-12')
        .once('glazed_free')
        .removeClass('col-sm-12')
        .addClass('col-sm-8 col-md-offset-2');

    $('.glazed-util-content-center-10-col .main-container', context).find('> .row > .col-sm-12')
        .once('glazed_free')
        .removeClass('col-sm-12')
        .addClass('col-sm-8 col-md-offset-1');

    // Breadcrumbs
    $('.breadcrumb a', context)
        .once('glazed_free')
        .after(' <span class="glazed_free-breadcrumb-spacer">/</span> ');


    // Sidebar nav blocks
    $('.region-sidebar-first .block .view ul, .region-sidebar-second .block .view ul', context)
        .once('glazed_free')
        .addClass('nav');

    // Portfolio content

    // Blog styling


    // Events styling
    $('.node-event [class^="field-event-"]', context)
        .once('glazed_free')
        .addClass('col-sm-9')
        .each(function() {
          $(this).prev().andSelf().wrapAll('<div class="row">');
        });

    $('.node-event .field-label', context)
        .once('glazed_free')
        .addClass('col-sm-3');

    $('.node-event .field-event-location', context)
        .once('glazed_free')
        .wrapInner('<a href="https://maps.google.com/?q=' + $('.node-event .field-event-location').text() + '">');
  }
};

// Create underscore debounce function if it doesn't exist already
if(typeof _ != 'function'){
  window._ = {};
  window._.debounce = function(func, wait, immediate) {
    var timeout, result;

    var later = function(context, args) {
      timeout = null;
      if (args) result = func.apply(context, args);
    };

    var debounced = restArgs(function(args) {
      var callNow = immediate && !timeout;
      if (timeout) clearTimeout(timeout);
      if (callNow) {
        timeout = setTimeout(later, wait);
        result = func.apply(this, args);
      } else if (!immediate) {
        timeout = _.delay(later, wait, this, args);
      }

      return result;
    });

    debounced.cancel = function() {
      clearTimeout(timeout);
      timeout = null;
    };

    return debounced;
  };
  var restArgs = function(func, startIndex) {
    startIndex = startIndex == null ? func.length - 1 : +startIndex;
    return function() {
      var length = Math.max(arguments.length - startIndex, 0);
      var rest = Array(length);
      for (var index = 0; index < length; index++) {
        rest[index] = arguments[index + startIndex];
      }
      switch (startIndex) {
        case 0: return func.call(this, rest);
        case 1: return func.call(this, arguments[0], rest);
        case 2: return func.call(this, arguments[0], arguments[1], rest);
      }
      var args = Array(startIndex + 1);
      for (index = 0; index < startIndex; index++) {
        args[index] = arguments[index];
      }
      args[startIndex] = rest;
      return func.apply(this, args);
    };
  }
  _.delay = restArgs(function(func, wait, args) {
    return setTimeout(function() {
      return func.apply(null, args);
    }, wait);
  });

}

$(window).resize(_.debounce(function(){
    if ($('#glazed_free-main-menu .menu').length > 0) {
      glazed_freeMenuGovernorBodyClass();
      glazed_freeMenuGovernor(document);
    }
}, 50));

function glazed_freeMenuGovernor(context) {
  // Bootstrap dropdown multi-column smart menu
  var navBreak = 1200;
  if('glazed_freeNavBreakpoint' in window) {
    navBreak = window.glazed_freeNavBreakpoint;
  }
  if (($('.body--glazed_free-header-side').length == 0) && $(window).width() > navBreak) {
    if (glazed_freeMenuState == 'top') {
      return false;
    }
    $('.html--glazed_free-nav-mobile--open').removeClass('html--glazed_free-nav-mobile--open');
    $('.glazed_free-header--side').removeClass('glazed_free-header--side').addClass('glazed_free-header--top');
    $('#glazed_free-main-menu .menu__breadcrumbs').remove();
    $('.menu__level').removeClass('menu__level').css('margin-top', 0).css('height', 'auto');
    $('.menu__item').removeClass('menu__item');
    $('[data-submenu]').removeAttr('data-submenu');
    $('[data-menu]').removeAttr('data-menu');

    var bodyWidth = $('body').innerWidth();
    var margin = 10;
    $('#glazed_free-main-menu .menu .dropdown-menu', context)
      .each(function() {
        var width = $(this).width();
        if ($(this).find('.glazed_free-megamenu__heading').length > 0) {
          var columns = $(this).find('.glazed_free-megamenu__heading').length;
        }
        else {
          var columns = Math.floor($(this).find('li').length / 8) + 1;
        }
        if (columns > 2) {
          $(this).css({
              'width' : '100%', // Full Width Mega Menu
              'left:' : '0',
          }).parent().css({
              'position' : 'static',
          }).find('.dropdown-menu >li').css({
              'width' : 100 / columns + '%',
          });
        }
        else {
          var $this = $(this);
          if (columns > 1) {
            // Accounts for 1px border.
            $this
              .css('min-width', width * columns + 2)
              .find('>li').css('width', width)
          }
          // Workaround for drop down overlapping.
          // See https://github.com/twbs/bootstrap/issues/13477.
          var $topLevelItem = $this.parent();
          // Set timeout to let the rendering threads catch up.
          setTimeout(function() {
            var delta = Math.round(bodyWidth - $topLevelItem.offset().left - $this.outerWidth() - margin);
            // Only fix items that went out of screen.
            if (delta < 0) {
              $this.css('left', delta + 'px');
            }
          }, 0)
        }
      });
    glazed_freeMenuState = 'top';
    // Hit Detection for Header
    if (($('.tabs--primary').length > 0) && ($('#navbar').length > 0)) {
      var tabsRect = $('.tabs--primary')[0].getBoundingClientRect();
      if (($('.glazed_free-header--navbar-pull-down').length > 0) && ($('#navbar .container-col').length > 0)) {
        var pullDownRect = $('#navbar .container-col')[0].getBoundingClientRect();
        if (glazed_freeHit(pullDownRect, tabsRect)) {
          $('.tabs--primary').css('margin-top', pullDownRect.bottom - tabsRect.top + 6);
        }
      }
      else {
        var navbarRect = $('#navbar')[0].getBoundingClientRect();
        if (glazed_freeHit(navbarRect, tabsRect)) {
          $('.tabs--primary').css('margin-top', navbarRect.bottom - tabsRect.top + 6);
        }

      }
    }
    if (($('#secondary-header').length > 0) && ($('#navbar.glazed_free-header--overlay').length > 0)) {
      var secHeaderRect = $('#secondary-header')[0].getBoundingClientRect();
      if (glazed_freeHit($('#navbar.glazed_free-header--overlay')[0].getBoundingClientRect(), secHeaderRect)) {
        $('#navbar.glazed_free-header--overlay').css('top', secHeaderRect.bottom);
      }
    }
  }
  // Mobile Menu with sliding panels and breadcrumb
  // @see glazed_free-mobile-nav.js
  else {
    if (glazed_freeMenuState == 'side') {
      return false;
    }
    // Temporary hiding while settings up @see #290
    $('#glazed_free-main-menu').hide();
    // Set up classes
    $('.glazed_free-header--top').removeClass('glazed_free-header--top').addClass('glazed_free-header--side');
    // Remove split-megamenu columns
    $('#glazed_free-main-menu .menu .dropdown-menu, #glazed_free-main-menu .menu .dropdown-menu li').removeAttr('style');
    $('#glazed_free-main-menu .menu').addClass('menu__level');
    $('#glazed_free-main-menu .menu .dropdown-menu').addClass('menu__level');
    $('#glazed_free-main-menu .menu .glazed_free-megamenu').addClass('menu__level');
    $('#glazed_free-main-menu .menu a').addClass('menu__link');
    $('#glazed_free-main-menu .menu li').addClass('menu__item');
    // Set up data attributes
    $('#glazed_free-main-menu .menu a.dropdown-toggle').each(function( index ) {
        $(this).attr('data-submenu', $(this).text())
          .next().attr('data-menu', $(this).text());
      });
    $('#glazed_free-main-menu .menu a.glazed_free-megamenu__heading').each(function( index ) {
        $(this).attr('data-submenu', $(this).text())
          .next().attr('data-menu', $(this).text());
      });

      var bc = ($('#glazed_free-main-menu .menu .dropdown-menu').length > 0);
      var menuEl = document.getElementById('glazed_free-main-menu'),
          mlmenu = new MLMenu(menuEl, {
              breadcrumbsCtrl : bc, // show breadcrumbs
              initialBreadcrumb : 'menu', // initial breadcrumb text
              backCtrl : false, // show back button
              itemsDelayInterval : 10, // delay between each menu item sliding animation
              // onItemClick: loadDummyData // callback: item that doesn´t have a submenu gets clicked - onItemClick([event], [inner HTML of the clicked item])
          });
      // mobile menu toggle
      $('#glazed_free-menu-toggle').once('glazed_freeMenuToggle').click(function() {
        $(this).toggleClass( 'navbar-toggle--active' );
        $(menuEl).toggleClass( 'menu--open' );
        $('html').toggleClass( 'html--glazed_free-nav-mobile--open' );
      });
      $('#glazed_free-main-menu').show();

      // See if logo  or block content overlaps menu and apply correction
      if ($('.wrap-branding').length > 0) {
        var brandingBottom = $('.wrap-branding')[0].getBoundingClientRect().bottom;
      }
      else {
        var brandingBottom = 0;
      }
      var $lastBlock = $('#glazed_free-main-menu .block:not(.block-menu)').last();

      // Show menu after completing setup
      // See if blocks overlap menu and apply correction
      if (($('.body--glazed_free-header-side').length > 0) && ($(window).width() > navBreak) && ($lastBlock.length > 0) && (brandingBottom > 0)) {
        $('#glazed_free-main-menu').css('padding-top', brandingBottom + 40);
      }
      if (($lastBlock.length > 0)) {
        var lastBlockBottom = $lastBlock[0].getBoundingClientRect().bottom;
        $('.menu__breadcrumbs').css('top', lastBlockBottom + 20);
        $('.menu__level').css('top', lastBlockBottom + 40);
        var offset = 40 + lastBlockBottom;
        $('.glazed_free-header--side .menu__level').css('height', 'calc(100vh - ' + offset + 'px)');
      }
      else if (($('.body--glazed_free-header-side').length > 0) && ($('.wrap-branding').length > 0) && (brandingBottom > 120)) {
        $('.menu__breadcrumbs').css('top', brandingBottom + 20);
        $('.menu__level').css('top', brandingBottom + 40);
        var offset = 40 + brandingBottom;
        $('.glazed_free-header--side .menu__level').css('height', 'calc(100vh - ' + offset + 'px)');
      }
    glazed_freeMenuState = 'side';
  }
}

function glazed_freeMenuGovernorBodyClass() {
  var navBreak = 1200;
  if('glazed_freeNavBreakpoint' in window) {
    navBreak = window.glazed_freeNavBreakpoint;
  }
  if ($(window).width() > navBreak) {
    $('.body--glazed_free-nav-mobile').removeClass('body--glazed_free-nav-mobile').addClass('body--glazed_free-nav-desktop');
  }
  else {
    $('.body--glazed_free-nav-desktop').removeClass('body--glazed_free-nav-desktop').addClass('body--glazed_free-nav-mobile');
  }
}

// Accepts 2 getBoundingClientRect objects
function glazed_freeHit(rect1, rect2) {
  return !(rect1.right < rect2.left ||
              rect1.left > rect2.right ||
              rect1.bottom < rect2.top ||
              rect1.top > rect2.bottom);
}

})(jQuery, Drupal, this, this.document);
