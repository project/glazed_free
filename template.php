<?php

/**
 * @file template.php
 */

/**
 * Load template.php logic from theme features
 */

foreach (file_scan_directory(drupal_get_path('theme', 'glazed_free') . '/features', '/controller.inc/i') as $file) {
  require_once($file->uri);
}
/**
 * Load template.php Glazed theme functions
 */

foreach (file_scan_directory(drupal_get_path('theme', 'glazed_free') . '/includes', '/.inc/i') as $file) {
  require_once($file->uri);
}

function glazed_freeNavFastInit() {
  if ($breakpoint = theme_get_setting('header_mobile_breakpoint')) {
    if ($breakpoint > 4099) {
      $breakpoint = 99999;
    }
  }
  else {
    $breakpoint = 1200;
  }
  $nav_init = <<<EOT
  <script>
  var glazed_freeNavBreakpoint = {$breakpoint};
  var glazed_freeWindowWidth = window.innerWidth;
  if (glazed_freeWindowWidth > glazed_freeNavBreakpoint) {
    document.body.className += ' body--glazed_free-nav-desktop';
  }
  else {
    document.body.className += ' body--glazed_free-nav-mobile';
  }
  </script>
EOT;
  return $nav_init;
}
